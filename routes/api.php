<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('users/seller', 'UserController@store');
Route::post('client/search', 'ClientController@search');
Route::post('users/search', 'UserController@search');

Route::post('login', 'AuthController@login');

Route::group(['middleware' => ['api','jwt.verify']], function ($router) {
    Route::resource('users', 'UserController')->only([
        'index', 'update', 'store', 'destroy'
    ]);

    Route::resource('client', 'ClientController')->only([
        'index', 'update', 'store', 'destroy'
    ]);

    Route::post('me', 'AuthController@me');
    Route::post('logout', 'AuthController@logout');

});

