<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;    

class PermisosSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // foreach ($this->permisos as $key) {
            Permission::create(['name' => 'create_user']);
            Permission::create(['name' => 'create_client']);
            Permission::create(['name' => 'update_user']);
            Permission::create(['name' => 'update_client']);
            Permission::create(['name' => 'delete_user']);
            Permission::create(['name' => 'delete_client']);
        // }
    }
}
