import Vue from 'vue';
import Router from 'vue-router';
import MenuView from '../layouts/MenuView.vue';
import Login from '../components/pages/login/Login';
import Home from '../components/pages/Home.vue';
import Users from '../components/list/Users.vue';
import Clients from '../components/list/Clients.vue';

// import ReportePersonas from '@/pages/reportes/reportePersonas'
// import Users from '@/pages/list/Users'
import {
    isTokenExpired,
    getCustomClaims
} from './helpers';

Vue.use(Router)
//const notify = this.$notification
function isAuth(to, from, next) {
    if (isTokenExpired()) {
        next('/login')
    } else {
        next()
    }
}

function isAdmin(to, from, next) {
    const claims = getCustomClaims()
    console.log(claims)
    const band = claims.is_admin
    if (band) {
        next()
    } else {
        alert('No posee permisos')
        next('/')
    }
}



export default new Router({
    routes: [{
            path: '/login',
            name: 'login',
            component: Login,
            invisible: true
        },
        {
            path: '/',
            beforeEnter: isAuth,
            beforeRouteEnter: isAuth,
            beforeResolve: isAuth,
            name: '',
            component: MenuView,
            icon: 'group',
            invisible: false,
            children: [{
                    path: '/',
                    beforeEnter: isAuth,
                    name: 'Inicio',
                    component: Home,
                    icon: 'form',
                    access: {
                        requiresLogin: true,
                        invisible: false
                    }
                },
                {
                    path: 'usuarios',
                    beforeEnter: isAuth,
                    name: 'Lista de usuarios',
                    component: Users,
                    icon: 'form',
                    access: {
                        requiresLogin: true,
                        invisible: false
                    }
                },
                {
                    path: 'clientes',
                    beforeEnter: isAuth,
                    name: 'Lista de clientes',
                    component: Clients,
                    icon: 'form',
                    access: {
                        requiresLogin: true,
                        invisible: false
                    }
                }
            ]
        }
    ]
})
